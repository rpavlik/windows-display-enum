// Copyright 2019, Collabora Ltd.
// SPDX-License-Identifier: BSL-1.0

#include "Interop.h"

#include <dxgi1_4.h>

#include <iostream>
using namespace winrt;

com_ptr<IDXGIAdapter> convertAdapter(
    Windows::Graphics::DisplayAdapterId adapterId) {
  com_ptr<IDXGIFactory1> factory;
  // CreateDXGIFactory1(__uuidof(IDXGIFactory1), factory.put_void());
  CreateDXGIFactory1(__uuidof(factory), factory.put_void());
  com_ptr<IDXGIFactory4> factory4 = factory.try_as<IDXGIFactory4>();
  if (!factory4) {
    std::cout << "Could not get IDXGIFactory4." << std::endl;
    return nullptr;
  }
  com_ptr<IDXGIAdapter> adapter;
  factory4->EnumAdapterByLuid(convertLUID(adapterId), __uuidof(adapter),
                              adapter.put_void());
  return adapter;
}
