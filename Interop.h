// Copyright 2019, Collabora Ltd.
// SPDX-License-Identifier: BSL-1.0

#pragma once

#include <Unknwn.h>
#include <winrt/Windows.Graphics.h>
#include <winrt/base.h>

#include <dxgi1_3.h>

winrt::com_ptr<IDXGIAdapter> convertAdapter(
    winrt::Windows::Graphics::DisplayAdapterId adapterId);

static inline LUID convertLUID(
    winrt::Windows::Graphics::DisplayAdapterId adapterId) {
  return {adapterId.LowPart, adapterId.HighPart};
}