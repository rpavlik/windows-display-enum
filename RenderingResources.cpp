// Copyright 2019, Collabora Ltd.
// SPDX-License-Identifier: BSL-1.0

#include "RenderingResources.h"

using namespace winrt;

com_ptr<IDXGIFactory1> factoryFromDevice(com_ptr<ID3D11Device> const& device) {
  auto dxgiDevice = device.as<IDXGIDevice>();
  com_ptr<IDXGIAdapter> adapter;
  check_hresult(dxgiDevice->GetAdapter(adapter.put()));
  com_ptr<IDXGIFactory1> factory;
  check_hresult(
      adapter->GetParent(guid_of<IDXGIFactory1>(), factory.put_void()));
  return factory;
}

RenderingResources::RenderingResources(com_ptr<IDXGIAdapter> const& adapter)
    : adapter_(adapter) {
  auto driverType = (nullptr == adapter_) ? D3D_DRIVER_TYPE_HARDWARE
                                          : D3D_DRIVER_TYPE_UNKNOWN;
  D3D_FEATURE_LEVEL acceptibleAPI = D3D_FEATURE_LEVEL_11_0;
  D3D_FEATURE_LEVEL foundAPI;

  UINT createDeviceFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
  check_hresult(D3D11CreateDevice(
      adapter_.get(), driverType, nullptr, createDeviceFlags, &acceptibleAPI, 1,
      D3D11_SDK_VERSION, device_.put(), &foundAPI, immediateContext_.put()));
  /*
  auto factory = factoryFromDevice(device_);
  factory->CreateSwapChain()
  auto dxgi111Device = device_.as<IDXGIDevice2>();

  DXGI_SWAP_CHAIN_DESC1 sd = {};
  sd.Width = 1920;
  sd.Height = 1080;
  sd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
  sd.SampleDesc.Count = 1;
  sd.SampleDesc.Quality = 0;
  sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
  sd.BufferCount = 1;
  dxg*/
}