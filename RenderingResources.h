// Copyright 2019, Collabora Ltd.
// SPDX-License-Identifier: BSL-1.0

#pragma once

#include <Unknwn.h>
#include <dxgi1_3.h>
#include <d3d11.h>
#include <winrt/base.h>

class RenderingResources {
 public:
  RenderingResources(winrt::com_ptr<IDXGIAdapter> const& adapter);

 private:
  winrt::com_ptr<IDXGIAdapter> adapter_;

  winrt::com_ptr<ID3D11DeviceContext> immediateContext_;
  winrt::com_ptr<ID3D11Device> device_;
};