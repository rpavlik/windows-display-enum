# Sample log

Interleaved

```
console output
```

and

> ```
> debug output
> ```

from running this tool, on a Windows 10 1903 install,
with a Radeon RX580 4GB with drivers 19.5.2.
Hardware connected include two matching Dell UltraSharp monitors,
plus an OSVR HDK2 that has been modified to report
the Microsoft-defined EDID extension flagging it as a
non-WinMR HMD.

----

```
Windows.Devices.Display.Core.DisplayManager interfaces:
- Windows::Devices::Display::Core::IDisplayManager
- IWeakReferenceSource
- {88fe2fba-af65-5bb9-b417-19c76803f487}
- Windows::Foundation::IClosable

   - disconnected
   - disconnected
   - disconnected
   - disconnected
   - disconnected
   - disconnected
   - disconnected
   - disconnected
\\?\DISPLAY#DEL413A#5&3779b283&9&UID264#{e6f07b5f-ee97-4a90-b076-33f57bf4eaa7}  DEL413A0WG2J84QB4PL_11_07E2_62 - connected -  UsageKind:standard
   Adapter path: \??\PCI#VEN_1002&DEV_67DF&SUBSYS_17011028&REV_C7#4&c204674&0&0019#{1ca05180-a699-450a-9a0c-de4fbe3ddd89} LUID: 57149
Try manipulating? (y/n) y



------- begin doStuffToTarget

Windows.Devices.Display.Core.DisplayDevice interfaces:
- Windows::Devices::Display::Core::IDisplayDevice
- IWeakReferenceSource
- {b1cabc53-2f31-5444-a6fa-ebf9aeb6bc6e}
- NOTE: QueryInterface for IDisplayDeviceInterop succeeded!

DisplayDevice::CreatePeriodicFence... OK!

Windows.Devices.Display.Core.DisplayFence interfaces:
- Windows::Devices::Display::Core::IDisplayFence
- IWeakReferenceSource

DisplayTarget::TryGetMonitor... OK!

Windows.Devices.Display.DisplayMonitor interfaces:
- Windows::Devices::Display::IDisplayMonitor
- IWeakReferenceSource

Native resolution: width 2560 x height 1440
DisplayDevice::CreatePrimary... OK!

Windows.Devices.Display.Core.DisplaySurface interfaces:
- Windows::Devices::Display::Core::IDisplaySurface
- IWeakReferenceSource

DisplayDevice::CreateScanoutSource... Got error: The text associated with this error code could not be found.
```

> ```
> 'display-enum.exe' (Win32): Loaded 'C:\Users\ryanp\src\windows-display-enum\x64\Debug\display-enum.exe'. Symbols loaded.
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\ntdll.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\kernel32.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\KernelBase.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\combase.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\ucrtbase.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\rpcrt4.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\bcryptprimitives.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\oleaut32.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\msvcp_win.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\dxgi.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\msvcrt.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\win32u.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\gdi32.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\gdi32full.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\user32.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\d3d11.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\kernel.appcore.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\msvcp140d.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\vcruntime140d.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\ucrtbased.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\DXCore.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\cfgmgr32.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\imm32.dll'. 
> The thread 0x5db4 has exited with code 0 (0x0).
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\clbcatq.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\DispBroker.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\sechost.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\ddisplay.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\WinTypes.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\ResourcePolicyClient.dll'. 
> 'display-enum.exe' (Win32): Unloaded 'C:\Windows\System32\ResourcePolicyClient.dll'
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\DriverStore\FileRepository\u0342855.inf_amd64_b40622b25c29110f\B342717\aticfx64.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\advapi32.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\version.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\winmm.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\winmmbase.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\winmmbase.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\winmmbase.dll'. 
> 'display-enum.exe' (Win32): Unloaded 'C:\Windows\System32\winmmbase.dll'
> 'display-enum.exe' (Win32): Unloaded 'C:\Windows\System32\winmmbase.dll'
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\dwmapi.dll'. 
> 'display-enum.exe' (Win32): Unloaded 'C:\Windows\System32\dwmapi.dll'
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\DriverStore\FileRepository\u0342855.inf_amd64_b40622b25c29110f\B342717\atiuxp64.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\DriverStore\FileRepository\u0342855.inf_amd64_b40622b25c29110f\B342717\atidxx64.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\setupapi.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\bcrypt.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\ole32.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\shell32.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\SHCore.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\windows.storage.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\profapi.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\powrprof.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\umpdc.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\shlwapi.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\cryptsp.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\apphelp.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\amdihk64.dll'. Module was built without symbols.
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\D3DSCache.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\twinapi.appcore.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\userenv.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\rmclient.dll'. 
> 'display-enum.exe' (Win32): Loaded 'C:\Windows\System32\Windows.Graphics.dll'. 
> onecoreuap\windows\core\dispbroker\lib\displaymanager.cpp(555)\DispBroker.dll!00007FFA068E7EC6: (caller: 00007FFA068E7C6C) Exception(1) tid(5ddc) D0000225     Msg:[Failed to create a source presentation handle] 
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: wil::ResultException at memory location 0x000000109916BCC0.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: [rethrow] at memory location 0x0000000000000000.
> onecoreuap\windows\core\dispbroker\lib\displaypath.cpp(695)\DispBroker.dll!00007FFA068EDFC5: (caller: 00007FF9C32D1E06) ReturnHr(1) tid(5ddc) D0000225     Msg:[onecoreuap\windows\core\dispbroker\lib\displaymanager.cpp(555)\DispBroker.dll!00007FFA068E7EC6: (caller: 00007FFA068E7C6C) Exception(1) tid(5ddc) D0000225     Msg:[Failed to create a source presentation handle] 
> ] 
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(175)\DDisplay.dll!00007FF9C32D2011: (caller: 00007FF9C32BA021) Exception(1) tid(5ddc) D0000225     CallContext:[\DeviceCreateScanoutSource] 
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: wil::ResultException at memory location 0x000000109916CE20.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: [rethrow] at memory location 0x0000000000000000.
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(179)\DDisplay.dll!00007FF9C32D8BA0: (caller: 00007FF9C32BA021) ReturnHr(1) tid(5ddc) D0000225     Msg:[onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(175)\DDisplay.dll!00007FF9C32D2011: (caller: 00007FF9C32BA021) Exception(1) tid(5ddc) D0000225     CallContext:[\DeviceCreateScanoutSource] 
> ] CallContext:[\DeviceCreateScanoutSource] 
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaydevice.cpp(128)\DDisplay.dll!00007FF9C32BA080: (caller: 00007FF74DEF8344) ReturnHr(2) tid(5ddc) D0000225 Exception thrown at 0x00007FFA104BA839 (KernelBase.dll) in display-enum.exe: WinRT originate error - 0xD0000225 : 'The text associated with this error code could not be found.'.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: winrt::hresult_error at memory location 0x000000109916E608.
> ```


```
------- end doStuffToTarget

\\?\DISPLAY#DEL413A#5&3779b283&9&UID265#{e6f07b5f-ee97-4a90-b076-33f57bf4eaa7}  DEL413A0WG2J85HBJNL_14_07E2_3F - connected -  UsageKind:standard
   Adapter path: \??\PCI#VEN_1002&DEV_67DF&SUBSYS_17011028&REV_C7#4&c204674&0&0019#{1ca05180-a699-450a-9a0c-de4fbe3ddd89} LUID: 57149
Try manipulating? (y/n) y



------- begin doStuffToTarget

Windows.Devices.Display.Core.DisplayDevice interfaces:
- Windows::Devices::Display::Core::IDisplayDevice
- IWeakReferenceSource
- {b1cabc53-2f31-5444-a6fa-ebf9aeb6bc6e}
- NOTE: QueryInterface for IDisplayDeviceInterop succeeded!

DisplayDevice::CreatePeriodicFence... OK!

Windows.Devices.Display.Core.DisplayFence interfaces:
- Windows::Devices::Display::Core::IDisplayFence
- IWeakReferenceSource

DisplayTarget::TryGetMonitor... OK!

Windows.Devices.Display.DisplayMonitor interfaces:
- Windows::Devices::Display::IDisplayMonitor
- IWeakReferenceSource

Native resolution: width 2560 x height 1440
DisplayDevice::CreatePrimary... OK!

Windows.Devices.Display.Core.DisplaySurface interfaces:
- Windows::Devices::Display::Core::IDisplaySurface
- IWeakReferenceSource

DisplayDevice::CreateScanoutSource... Got error: The text associated with this error code could not be found.
```

Monitor, and exception stuff, same as previous.

```

------- end doStuffToTarget

   - disconnected
   - disconnected
\\?\DISPLAY#SVR1019#5&3779b283&9&UID268#{e6f07b5f-ee97-4a90-b076-33f57bf4eaa7}  SVR1019Sensics HDK2_00_07E0_2D - connected -  UsageKind:head mounted
   Adapter path: \??\PCI#VEN_1002&DEV_67DF&SUBSYS_17011028&REV_C7#4&c204674&0&0019#{1ca05180-a699-450a-9a0c-de4fbe3ddd89} LUID: 57149
Try manipulating? (y/n) y



------- begin doStuffToTarget

Windows.Devices.Display.Core.DisplayDevice interfaces:
- Windows::Devices::Display::Core::IDisplayDevice
- IWeakReferenceSource
- {b1cabc53-2f31-5444-a6fa-ebf9aeb6bc6e}
- NOTE: QueryInterface for IDisplayDeviceInterop succeeded!

DisplayDevice::CreatePeriodicFence... Got error: The parameter is incorrect.
DisplayTarget::TryGetMonitor... OK!

Windows.Devices.Display.DisplayMonitor interfaces:
- Windows::Devices::Display::IDisplayMonitor
- IWeakReferenceSource

Native resolution: width 2160 x height 1200
DisplayDevice::CreatePrimary... Got error: Element not found.
```

> ```
> onecoreuap\windows\directx\dxg\ddisplay\dll\DependencyWrapper.h(31)\DDisplay.dll!00007FF9C32BC572: (caller: 00007FF74DEF8083) Exception(3) tid(5ddc) 80070057 The parameter is incorrect.
>     Msg:[Error calling dependency] 
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: wil::ResultException at memory location 0x000000109916AEA0.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: [rethrow] at memory location 0x0000000000000000.
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaydevice.cpp(705)\DDisplay.dll!00007FF9C32D8492: (caller: 00007FF74DEF8083) ReturnHr(5) tid(5ddc) 80070057 The parameter is incorrect.
>     Msg:[onecoreuap\windows\directx\dxg\ddisplay\dll\DependencyWrapper.h(31)\DDisplay.dll!00007FF9C32BC572: (caller: 00007FF74DEF8083) Exception(3) tid(5ddc) 80070057 The parameter is incorrect.
>     Msg:[Error calling dependency] 
> ] 
> Exception thrown at 0x00007FFA104BA839 (KernelBase.dll) in display-enum.exe: WinRT originate error - 0x80070057 : 'The parameter is incorrect.'.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: winrt::hresult_invalid_argument at memory location 0x000000109916E428.
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysurface.cpp(73)\DDisplay.dll!00007FF9C32CC477: (caller: 00007FF9C32BA660) Exception(4) tid(5ddc) 80070490 Element not found.
>     Msg:[Could not create a primary for an inactive target.] CallContext:[\DeviceCreatePrimary] 
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: wil::ResultException at memory location 0x000000109916AD30.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: [rethrow] at memory location 0x0000000000000000.
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysurface.cpp(110)\DDisplay.dll!00007FF9C32D89EA: (caller: 00007FF9C32BA660) ReturnHr(6) tid(5ddc) 80070490 Element not found.
>     Msg:[onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysurface.cpp(73)\DDisplay.dll!00007FF9C32CC477: (caller: 00007FF9C32BA660) Exception(4) tid(5ddc) 80070490 Element not found.
>     Msg:[Could not create a primary for an inactive target.] CallContext:[\DeviceCreatePrimary] 
> ] CallContext:[\DeviceCreatePrimary] 
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaydevice.cpp(208)\DDisplay.dll!00007FF9C32BA6C1: (caller: 00007FF74DEF81F3) ReturnHr(7) tid(5ddc) 80070490 Element not found.
>     CallContext:[\DeviceCreatePrimary] 
> Exception thrown at 0x00007FFA104BA839 (KernelBase.dll) in display-enum.exe: WinRT originate error - 0x80070490 : 'Element not found.'.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: winrt::hresult_error at memory location 0x000000109916E608.
> ```

```

DisplayDevice::CreateScanoutSource... Got error: Element not found.

```

> ```
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(167)\DDisplay.dll!00007FF9C32D1FE7: (caller: 00007FF9C32BA021) Exception(5) tid(5ddc) 80070490 Element not found.
>     CallContext:[\DeviceCreateScanoutSource] 
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: wil::ResultException at memory location 0x000000109916CE20.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: [rethrow] at memory location 0x0000000000000000.
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(179)\DDisplay.dll!00007FF9C32D8BA0: (caller: 00007FF9C32BA021) ReturnHr(8) tid(5ddc) 80070490 Element not found.
>     Msg:[onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(167)\DDisplay.dll!00007FF9C32D1FE7: (caller: 00007FF9C32BA021) Exception(5) tid(5ddc) 80070490 Element not found.
>     CallContext:[\DeviceCreateScanoutSource] 
> ] CallContext:[\DeviceCreateScanoutSource] 
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaydevice.cpp(128)\DDisplay.dll!00007FF9C32BA080: (caller: 00007FF74DEF8344) ReturnHr(9) tid(5ddc) 80070490 Element not found.
> Exception thrown at 0x00007FFA104BA839 (KernelBase.dll) in display-enum.exe: WinRT originate error - 0x80070490 : 'Element not found.'.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: winrt::hresult_error at memory location 0x000000109916E608.
> ```

```

------- end doStuffToTarget

   - disconnected
   - disconnected
   - disconnected

Windows.Devices.Display.Core.DisplayTarget interfaces:
- Windows::Devices::Display::Core::IDisplayTarget
- IWeakReferenceSource

Trying to acquire HMD:
OK! (No idea what that means, but...)

Windows.Devices.Display.Core.DisplayState interfaces:
- Windows::Devices::Display::Core::IDisplayState
- IWeakReferenceSource


Windows.Devices.Display.Core.DisplayPath interfaces:
- Windows::Devices::Display::Core::IDisplayPath
- IDisplayPathInterop
- IWeakReferenceSource
- {00000000-0000-0000-c000-000000000046}
- Windows::Foundation::IInspectable


Windows.Devices.Display.Core.DisplayDevice interfaces:
- Windows::Devices::Display::Core::IDisplayDevice
- IWeakReferenceSource
- {b1cabc53-2f31-5444-a6fa-ebf9aeb6bc6e}
- NOTE: QueryInterface for IDisplayDeviceInterop succeeded!

DisplayDevice::CreatePeriodicFence... Got error: The parameter is incorrect.
```

> ```
> onecoreuap\windows\directx\dxg\ddisplay\dll\DependencyWrapper.h(31)\DDisplay.dll!00007FF9C32BC572: (caller: 00007FF74DEF8083) Exception(6) tid(5ddc) 80070057 The parameter is incorrect.
>     Msg:[Error calling dependency] 
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: wil::ResultException at memory location 0x000000109916B2E0.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: [rethrow] at memory location 0x0000000000000000.
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaydevice.cpp(705)\DDisplay.dll!00007FF9C32D8492: (caller: 00007FF74DEF8083) ReturnHr(10) tid(5ddc) 80070057 The parameter is incorrect.
>     Msg:[onecoreuap\windows\directx\dxg\ddisplay\dll\DependencyWrapper.h(31)\DDisplay.dll!00007FF9C32BC572: (caller: 00007FF74DEF8083) Exception(6) tid(5ddc) 80070057 The parameter is incorrect.
>     Msg:[Error calling dependency] 
> ] 
> Exception thrown at 0x00007FFA104BA839 (KernelBase.dll) in display-enum.exe: WinRT originate error - 0x80070057 : 'The parameter is incorrect.'.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: winrt::hresult_invalid_argument at memory location 0x000000109916E868.
> ```

```
DisplayTarget::TryGetMonitor... OK!

Windows.Devices.Display.DisplayMonitor interfaces:
- Windows::Devices::Display::IDisplayMonitor
- IWeakReferenceSource

Native resolution: width 2160 x height 1200
DisplayDevice::CreatePrimary... Got error: Element not found.
```

> ```
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysurface.cpp(73)\DDisplay.dll!00007FF9C32CC477: (caller: 00007FF9C32BA660) Exception(7) tid(5ddc) 80070490 Element not found.
>     Msg:[Could not create a primary for an inactive target.] CallContext:[\DeviceCreatePrimary] 
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: wil::ResultException at memory location 0x000000109916B170.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: [rethrow] at memory location 0x0000000000000000.
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysurface.cpp(110)\DDisplay.dll!00007FF9C32D89EA: (caller: 00007FF9C32BA660) ReturnHr(11) tid(5ddc) 80070490 Element not found.
>     Msg:[onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysurface.cpp(73)\DDisplay.dll!00007FF9C32CC477: (caller: 00007FF9C32BA660) Exception(7) tid(5ddc) 80070490 Element not found.
>     Msg:[Could not create a primary for an inactive target.] CallContext:[\DeviceCreatePrimary] 
> ] CallContext:[\DeviceCreatePrimary] 
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaydevice.cpp(208)\DDisplay.dll!00007FF9C32BA6C1: (caller: 00007FF74DEF81F3) ReturnHr(12) tid(5ddc) 80070490 Element not found.
>     CallContext:[\DeviceCreatePrimary] 
> Exception thrown at 0x00007FFA104BA839 (KernelBase.dll) in display-enum.exe: WinRT originate error - 0x80070490 : 'Element not found.'.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: winrt::hresult_error at memory location 0x000000109916EA48.
> ```

```
DisplayDevice::CreateScanoutSource... Got error: Element not found.
```

> ```
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(167)\DDisplay.dll!00007FF9C32D1FE7: (caller: 00007FF9C32BA021) Exception(8) tid(5ddc) 80070490 Element not found.
>     CallContext:[\DeviceCreateScanoutSource] 
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: wil::ResultException at memory location 0x000000109916D260.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: [rethrow] at memory location 0x0000000000000000.
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(179)\DDisplay.dll!00007FF9C32D8BA0: (caller: 00007FF9C32BA021) ReturnHr(13) tid(5ddc) 80070490 Element not found.
>     Msg:[onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaysource.cpp(167)\DDisplay.dll!00007FF9C32D1FE7: (caller: 00007FF9C32BA021) Exception(8) tid(5ddc) 80070490 Element not found.
>     CallContext:[\DeviceCreateScanoutSource] 
> ] CallContext:[\DeviceCreateScanoutSource] 
> onecoreuap\windows\directx\dxg\ddisplay\dll\ddisplaydevice.cpp(128)\DDisplay.dll!00007FF9C32BA080: (caller: 00007FF74DEF8344) ReturnHr(14) tid(5ddc) 80070490 Element not found.
> Exception thrown at 0x00007FFA104BA839 (KernelBase.dll) in display-enum.exe: WinRT originate error - 0x80070490 : 'Element not found.'.
> Exception thrown at 0x00007FFA104BA839 in display-enum.exe: Microsoft C++ exception: winrt::hresult_error at memory location 0x000000109916EA48.
> ```

```
  Output: \\.\DISPLAY1
    Attached: yes
  Output: \\.\DISPLAY2
    Attached: yes
```