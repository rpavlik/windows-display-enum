﻿// Copyright 2019, Collabora Ltd.
// SPDX-License-Identifier: BSL-1.0

#include "Interop.h"
#include "RenderingResources.h"
#include "pch.h"

#include <Windows.Devices.Display.Core.Interop.h>
#include <winrt/Windows.UI.Composition.h>
#include "weakreference.h"

#include <chrono>
#include <iostream>
#include <vector>

using namespace std::literals;
using namespace winrt;
using namespace Windows::Foundation;
using namespace Windows::Devices::Display;
using Windows::Devices::Display::Core::DisplayManager;
using Windows::Devices::Display::Core::DisplayManagerOptions;
using Windows::Devices::Display::Core::DisplayTarget;
using Windows::Graphics::DirectX::Direct3D11::Direct3DMultisampleDescription;
using namespace Windows::Graphics::DirectX;
// static com_ptr<IDXGIAdapter> convertAdapter(Windows::)

static void dumpInterfaces(Windows::Foundation::IInspectable const& obj) {
  if (!obj) {
    std::cout << "Skipping dumpInterfaces for a null object" << std::endl;
    return;
  }

  std::wcout << std::endl
             << winrt::get_class_name(obj).c_str()
             << " interfaces:" << std::endl;
  for (auto&& iid : winrt::get_interfaces(obj)) {
    std::wcout << L"- ";
#define TRY_INTERFACE(NAME)            \
  if (iid == winrt::guid_of<NAME>()) { \
    std::cout << #NAME "\n";           \
    continue;                          \
  }

#define TRY_NON_WINRT_INTERFACE(NAME)                                 \
  if (iid == winrt::guid_of<NAME>() || GUID(iid) == __uuidof(NAME)) { \
    std::cout << #NAME "\n";                                          \
    continue;                                                         \
  }
    // Not sure why this would appear on some but not all...
    TRY_INTERFACE(Windows::Foundation::IInspectable);
    TRY_NON_WINRT_INTERFACE(IWeakReferenceSource);
    TRY_NON_WINRT_INTERFACE(IWeakReference);

    TRY_NON_WINRT_INTERFACE(IDXGIAdapter);
    TRY_NON_WINRT_INTERFACE(IDXGIAdapter1);
    TRY_NON_WINRT_INTERFACE(IDXGIAdapter2);
    TRY_NON_WINRT_INTERFACE(IDXGIAdapter3);
    TRY_NON_WINRT_INTERFACE(IDXGIDevice);
    TRY_NON_WINRT_INTERFACE(IDXGIDevice1);
    TRY_NON_WINRT_INTERFACE(IDXGIDevice2);
    TRY_NON_WINRT_INTERFACE(IDXGIDevice3);
    TRY_NON_WINRT_INTERFACE(IDXGIDeviceSubObject);
    TRY_NON_WINRT_INTERFACE(IDXGISurface);

    TRY_NON_WINRT_INTERFACE(IDisplayDeviceInterop);
    TRY_NON_WINRT_INTERFACE(IDisplayPathInterop);

    TRY_INTERFACE(Windows::Foundation::IClosable);
    TRY_INTERFACE(Windows::Devices::Display::IDisplayMonitor);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplayAdapter);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplayDevice);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplayManager);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplayTarget);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplaySurface);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplaySource);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplayPath);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplayFence);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplayScanout);
    TRY_INTERFACE(Windows::Devices::Display::Core::IDisplayState);
    TRY_INTERFACE(Windows::Graphics::DirectX::Direct3D11::IDirect3DDevice);
    TRY_INTERFACE(Windows::Graphics::DirectX::Direct3D11::IDirect3DSurface);

    std::wcout << winrt::to_hstring(iid).c_str() << std::endl;
  }

#define TRY_QI(NAME)                                                  \
  if (obj.try_as<NAME>()) {                                           \
    std::cout << "- NOTE: QueryInterface for " #NAME " succeeded!\n"; \
  }

  TRY_QI(IDisplayDeviceInterop);
  TRY_QI(IDXGIDevice);
  TRY_QI(IDXGISurface);
  TRY_QI(IDXGIAdapter);
  TRY_QI(IDXGISwapChain);
  TRY_QI(Windows::Graphics::DirectX::Direct3D11::IDirect3DDevice);
  TRY_QI(Windows::Graphics::DirectX::Direct3D11::IDirect3DSurface);

  std::wcout << std::endl;
}
static void handle_hresult_error(winrt::hresult_error const& e) {
  std::cout << "Got error: ";
  std::wcout << e.message().c_str() << std::endl;
}

static int doStuffToTarget(DisplayManager const& dm,
                           DisplayTarget const& target) {
  Core::DisplayDevice dispDevice = dm.CreateDisplayDevice(target.Adapter());
  if (!dispDevice) {
    std::cout << "Couldn't create display device" << std::endl;
    return 0;
  }

  // Has the following IID that I can't find documented:
  // {b1cabc53-2f31-5444-a6fa-ebf9aeb6bc6e}
  dumpInterfaces(dispDevice);

  // Not advertised in the implemented interfaces list, but it works...
  auto ddi = dispDevice.as<IDisplayDeviceInterop>();
// Now, what can I convert using this? signature for CreateSharedHandle takes
// an IInspectable and outputs a HANDLE. Docs mention doing this to fences,
// but I can't create a fence...
// Would be handy if I could use it as a HWND to create a swapchain on, but
// that seems unlikely to work.
#if 0
  {
	  // Can't get an ABI type because ABI header missing for windows.devices.display.core
	  ddi->CreateSharedHandle(target ??)
  }
#endif

  // On HMDs, fails with "The parameter is incorrect" and inner message "Error
  // calling dependency" - so can't try IDisplayDeviceInterop per the docs
  std::cout << "DisplayDevice::CreatePeriodicFence... ";
  try {
    Core::DisplayFence fence = dispDevice.CreatePeriodicFence(target, 1ms);
    std::cout << "OK!" << std::endl;
    dumpInterfaces(fence);
  } catch (winrt::hresult_error const& e) {
    handle_hresult_error(e);
  }

  std::cout << "DisplayTarget::TryGetMonitor... ";
  DisplayMonitor monitor = target.TryGetMonitor();
  if (!monitor) {
    std::cout << "Did not succeed!" << std::endl;
  } else {
    std::cout << "OK!" << std::endl;
    dumpInterfaces(monitor);

    auto res = monitor.NativeResolutionInRawPixels();
    std::cout << "Native resolution: width " << res.Width << " x height "
              << res.Height << std::endl;
    auto pixFormat = DirectXPixelFormat::R8G8B8A8UIntNormalized;
    // sRGB
    auto colorSpace = DirectXColorSpace::RgbFullG22NoneP709;
    Direct3D11::Direct3DMultisampleDescription multisampleDescription{1, 0};
    Core::DisplayPrimaryDescription dispPrimaryDesc{
        static_cast<uint32_t>(res.Width),
        static_cast<uint32_t>(res.Height),
        pixFormat,
        colorSpace,
        false,
        multisampleDescription};

    // Always fails on HMDs, saying that the display isn't active so we can't
    // use it... but that's kind of the point, we want to use the inactive HMD
    // display.
    // Succeeds on normal displays that are in use, though.
    std::cout << "DisplayDevice::CreatePrimary... ";
    try {
      Core::DisplaySurface surface =
          dispDevice.CreatePrimary(target, dispPrimaryDesc);
      std::cout << "OK!" << std::endl;
      dumpInterfaces(surface);
    } catch (winrt::hresult_error const& e) {
      handle_hresult_error(e);
    }
  }

  // always fails with "Element not found" for HMDs, and "Failed to create a
  // source presentation handle" for monitors
  std::cout << "DisplayDevice::CreateScanoutSource... ";
  try {
    Core::DisplaySource source = dispDevice.CreateScanoutSource(target);
    dumpInterfaces(source);
    std::cout << "OK!" << std::endl;
  } catch (winrt::hresult_error const& e) {
    handle_hresult_error(e);
  }
  return 0;
}

static std::vector<DisplayTarget> enumerateAndGetHMDTargets(DisplayManager dm) {
  std::vector<DisplayTarget> hmdTargets;
  for (auto&& target : dm.GetCurrentTargets()) {
    std::wcout << target.DeviceInterfacePath().c_str();
    std::cout << "  ";
    std::wcout << target.StableMonitorId().c_str();

    if (target.IsConnected()) {
      std::cout << " - connected - ";
    } else {
      std::cout << " - disconnected" << std::endl;
      continue;
    }

    std::cout << " UsageKind:";
    using namespace Windows::Devices::Display;

    switch (target.UsageKind()) {
      case DisplayMonitorUsageKind::Standard:
        std::cout << "standard";
        break;
      case DisplayMonitorUsageKind::HeadMounted:
        std::cout << "head mounted";
        hmdTargets.emplace_back(target);
        break;
      case DisplayMonitorUsageKind::SpecialPurpose:
        std::cout << "special purpose";
        break;
    }

    auto adapter = target.Adapter();
    std::cout << std::endl << "   Adapter path: ";

    std::wcout << adapter.DeviceInterfacePath().c_str();
    auto adapterId = adapter.Id();
    std::cout << " LUID: " << Int64FromLuid(convertLUID(adapterId))
              << std::endl;

    std::string answer;
    while (answer != "y" && answer != "n") {
      std::cout << "Try manipulating? (y/n) ";
      std::cin >> answer;
      std::cout << std::endl;
    }
    if (answer == "y") {
      std::cout << "\n\n------- begin doStuffToTarget" << std::endl;
      doStuffToTarget(dm, target);

      std::cout << "\n\n------- end doStuffToTarget\n" << std::endl;
    }
  }
  return hmdTargets;
}

static void handleAcquireResult(
    Windows::Devices::Display::Core::DisplayManagerResult acquireResult) {
  switch (acquireResult) {
    case Core::DisplayManagerResult::RemoteSessionNotSupported:
      std::cout << "RemoteSessionNotSupported" << std::endl;
      break;
    case Core::DisplayManagerResult::TargetAccessDenied:
      std::cout << "TargetAccessDenied" << std::endl;
      break;
    case Core::DisplayManagerResult::TargetStale:
      std::cout << "TargetStale" << std::endl;
      break;
    case Core::DisplayManagerResult::UnknownFailure:
      std::cout << "UnknownFailure" << std::endl;
      break;
    case Core::DisplayManagerResult::Success:
      std::cout << "Success" << std::endl;
      break;
  }
}

std::vector<com_ptr<IDXGIOutput>> getOutputs(
    com_ptr<IDXGIAdapter> const& adapter) {
  std::vector<com_ptr<IDXGIOutput>> ret;
  for (UINT index = 0; index < 1024; ++index) {
    com_ptr<IDXGIOutput> output;
    auto result = adapter->EnumOutputs(index, output.put());
    if (result == DXGI_ERROR_NOT_FOUND) {
      break;
    }
    winrt::check_hresult(result);
    ret.emplace_back(std::move(output));
  }
  return ret;
}

int main() {
  winrt::init_apartment();

  DisplayManager dm{nullptr};
  try {
    dm = DisplayManager::Create(DisplayManagerOptions::None);
  } catch (hresult_error const& e) {
    if (e.code() == REGDB_E_CLASSNOTREG) {
      std::cerr << "Class not registered - probably too old of windows (Need "
                   "1809 at least)"
                << std::endl;
    } else {
      std::cerr << "Some error creating displaymanager!" << std::endl;
      std::wcerr << e.message().c_str() << std::endl;
    }
    return 0;
  }
  // Has the following IID that I can't find documented:
  // {88fe2fba-af65-5bb9-b417-19c76803f487}
  dumpInterfaces(dm);
  std::vector<DisplayTarget> hmdTargets = enumerateAndGetHMDTargets(dm);
  if (hmdTargets.empty()) {
    std ::cout << "No HMD detected, all done." << std::endl;
    return 0;
  }
  Core::DisplayTarget hmdTarget = hmdTargets.front();
  dumpInterfaces(hmdTarget);

  std::cout << "Trying to acquire HMD:" << std::endl;

#if 0
  auto acquireResult = dm.TryAcquireTarget(hmdTarget);
  if (acquireResult != Core::DisplayManagerResult::Success) {
    return 0;
  }
#endif

  auto targets{winrt::single_threaded_vector<DisplayTarget>({hmdTarget})};

  auto resultWithState = dm.TryAcquireTargetsAndCreateEmptyState(targets);
  winrt::check_hresult(resultWithState.ExtendedErrorCode());
  std::cout << "OK! (No idea what that means, but...)" << std::endl;

  Core::DisplayState state = resultWithState.State();
  dumpInterfaces(state);

  // Has the following IID that is apparently for IWeakReference, but isn't
  // getting recognized as such by dumpInterfaces():
  // {00000000-0000-0000-c000-000000000046}
  Core::DisplayPath path = state.ConnectTarget(hmdTarget);
  dumpInterfaces(path);

  doStuffToTarget(dm, hmdTarget);

  com_ptr<IDXGIAdapter> dxgiAdapter = convertAdapter(hmdTarget.Adapter().Id());
  if (dxgiAdapter) {
    auto outputs = getOutputs(dxgiAdapter);
    // HMD never shows up here...
    for (auto&& output : outputs) {
      DXGI_OUTPUT_DESC desc;
      winrt::check_hresult(output->GetDesc(&desc));
      std::wcout << L"  Output: " << desc.DeviceName << std::endl;
      std::wcout << L"    Attached: "
                 << (desc.AttachedToDesktop ? L"yes" : L"no") << std::endl;
    }

    // How to get something to render with, from the various
    // Windows.Devices.Display.Core objects?
    // Closest I've gotten so far was getting the same adapter, via LUID.
    // I suspect the elusive "IDisplayDeviceInterop" interface has the answer...
    // https://github.com/MicrosoftDocs/winrt-api/issues/1211
    RenderingResources resources{dxgiAdapter};
  }

  dm.ReleaseTarget(hmdTarget);
}
