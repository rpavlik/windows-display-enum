﻿// Copyright 2019, Collabora Ltd.
// SPDX-License-Identifier: BSL-1.0

#pragma once
#include <Unknwn.h>
#include <winrt/Windows.Foundation.h>
#include <winrt/Windows.Foundation.Collections.h>
#include <winrt/Windows.Devices.Display.Core.h>


#include "dxgi1_4.h"