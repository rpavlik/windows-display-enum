# Display enumeration

Trying to enumerate and use non-desktop displays (HMDs) in "direct mode" without any NDA APIs.

This is being shared in the hopes that the gaps can be more easily filled in through collaboration.

## Links

- <https://docs.microsoft.com/en-us/uwp/api/windows.devices.display.core>
- <https://social.msdn.microsoft.com/Forums/officeocs/en-US/607101d6-b044-4bec-8986-1cf21257fe74/presenting-content-to-a-displaysurface-using-windowsdevicesdisplaycore?forum=wpdevelop>

## Status

- Blocked on apparent gaps in the API as documented: 
  Can't connect between this API and Direct3D/DXGI/DXCore to be able to render to a display.
- Missing ABI header for Windows.Devices.Display.Core: only the winmd-generated cppwinrt header is provided.
- Can't execute some seemingly-required calls on an HMD device.

Unidentified IIDs:

- 88fe2fba-af65-5bb9-b417-19c76803f487 - implemented by Windows.Devices.Display.Core.DisplayManager
- b1cabc53-2f31-5444-a6fa-ebf9aeb6bc6e - implemented by Windows.Devices.Display.Core.DisplayDevice

## Classes and Interfaces

Interfaces in general were determined using the `IInspectable::GetIids()` method
via `winrt::get_interfaces()`, then comparing each IID retrieved against the
IIDs of a number of interfaces of interest.

- Windows.Devices.Display.Core.DisplayManager interfaces:
  - Windows.Devices.Display.Core.IDisplayManager
  - IWeakReferenceSource
  - **Unknown: {88fe2fba-af65-5bb9-b417-19c76803f487}**
  - Windows.Foundation.IClosable

- Windows.Devices.Display.Core.DisplayDevice interfaces:
  - Windows.Devices.Display.Core.IDisplayDevice
  - IWeakReferenceSource
  - **Unknown: {b1cabc53-2f31-5444-a6fa-ebf9aeb6bc6e}**
  - IDisplayDeviceInterop (**via QueryInterface - unadvertised in GetIids**)

- Windows.Devices.Display.DisplayMonitor interfaces:
  - Windows.Devices.Display.IDisplayMonitor
  - IWeakReferenceSource

- Windows.Devices.Display.Core.DisplayFence interfaces:
  - Windows.Devices.Display.Core.IDisplayFence
  - IWeakReferenceSource

- Windows.Devices.Display.Core.DisplaySurface interfaces:
  - Windows.Devices.Display.Core.IDisplaySurface
  - IWeakReferenceSource

- Windows.Devices.Display.Core.DisplayState interfaces:
  - Windows.Devices.Display.Core.IDisplayState
  - IWeakReferenceSource

- Windows.Devices.Display.Core.DisplayPath interfaces:
  - Windows.Devices.Display.Core.IDisplayPath
  - IDisplayPathInterop
  - IWeakReferenceSource
  - {00000000-0000-0000-c000-000000000046} (**IWeakReference?**)
  - Windows.Foundation.IInspectable (**Why is this the only class reporting this interface?**)
